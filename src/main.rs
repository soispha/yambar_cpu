use std::{thread, time::Duration};

use sysinfo::{CpuExt, System, SystemExt};

fn main() {
    let mut sys = System::new();

    // Number of CPUs:
    loop {
        sys.refresh_cpu();
        let cpu_usage: f32 = sys.cpus().iter().map(|cpu| cpu.cpu_usage()).sum();
        println!("cpu|range:0-100|{:.0}", cpu_usage / sys.cpus().iter().count() as f32);
        println!();

        // Sleeping to give the system time to run for long
        // enough to have useful information.
        thread::sleep(Duration::from_secs(3));
    }
}
