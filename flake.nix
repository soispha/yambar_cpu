{
  description = "A smart way to check your cpu usage";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    crane.url = "github:ipetkov/crane";
    crane.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    crane,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      craneLib = crane.lib.${system};
      craneBuild = craneLib.buildPackage {
        src = craneLib.cleanCargoSource ./.;

        doCheck = true;
      };
    in {
      packages.default = craneBuild;
      legacyPackages.default = craneBuild;
      app.default = {
        type = "app";
        program = "${self.packages.${system}.default}/bin/cpu_usage";
      };
    });
}
# vim: ts=2

